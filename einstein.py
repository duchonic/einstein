import pygame
import math
import random
from pygame_gui.elements import UIButton
from pygame_gui import UIManager, UI_BUTTON_PRESSED

pygame.init()


BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREY = (128, 128, 128)
RED = [255, 0, 0]
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)

COLOR_LIST = [RED, BLUE, GREEN]

class Options:
    def __init__(self):
        self.resolution = (800, 600)
        self.fullscreen = False
        self.radius = 30

class Position:
    def __init__(self, position, side) -> None:
        self.x = position[0] 
        self.y = position[1]
        self.side = side
        self.height = side/2 * math.sqrt(3)

    def get_pos(self) -> (float, float):
        return (self.get_x(), self.get_y())

    def get_x(self) -> float:
        return self.side * self.x * 1.5
    def get_y(self) -> float:
        if self.x%2:
            return self.y * 2 * (self.side/2 * math.sqrt(3))
        else:
            return self.y * 2 * (self.side/2 * math.sqrt(3)) + (self.side/2 * math.sqrt(3))
    
    def print(self) -> None:
        print("x:", self.x, " y:", self.y)

    

def draw_hexagon(Surface, color, radius, position):

    pos = Position(position, radius)

    pi = math.pi
    pi2 = 2*pi
    n = 6
    for i in range(0,6):
        edges = (math.cos((pi/2) +i / n * pi2) * radius + pos.get_x(), math.sin((pi/2) +i / n * pi2) * radius + pos.get_y())
        pygame.draw.line(Surface, color, pos.get_pos(), edges )

    corners = [(math.cos( i / n * pi2) * radius + pos.get_x(), math.sin(  i / n * pi2) * radius + pos.get_y()) for i in range(0, n)]
    pygame.draw.lines(Surface, color, True, corners)


def get_neigbours(position, rotation=0):
    neigbours = [position]

    if rotation == 0:
        if position[0]%2:
            neigbours.append( (position[0]+1, position[1]-1))
            neigbours.append( (position[0]+1, position[1]) )
        else:
            neigbours.append( (position[0]+1, position[1]))
            neigbours.append( (position[0]+1, position[1]+1))
    elif rotation == 1:
        if position[0]%2:
            neigbours.append( (position[0], position[1]+1))
            neigbours.append( (position[0]-1, position[1]))
        else:
            neigbours.append( (position[0], position[1]+1 ))
            neigbours.append( (position[0]-1, position[1]+1 ))
    elif rotation == 2:
        if position[0]%2:
            neigbours.append( (position[0]-1, position[1]-1))
            neigbours.append( (position[0], position[1]-1))
        else:
            neigbours.append( (position[0]-1, position[1]))
            neigbours.append( (position[0], position[1]-1))
    
    return neigbours

def draw_spectre(Surface, color, radius, position, rotation=0):


    # normal hat
    for index, pos in enumerate(get_neigbours(position, rotation)):
        if index == 0:
            draw_segment(Surface, color, radius, pos, [0,1,2,5], rotation)
        if index == 1:
            draw_segment(Surface, color, radius, pos, [2,3], rotation)
        if index == 2:
            draw_segment(Surface, color, radius, pos, [4,5], rotation)


def draw_segment(Surface, color, radius, position, segment, rotation=0):

    pos = Position(position, radius)

    for seg in segment:

        seg += rotation*2
        seg %= 6

        pi = math.pi
        pi2 = 2*pi
        n = 6
        # the center
        corners = [ pos.get_pos() ]
        corners.append( (math.cos((pi/2) + (seg-2) / n * pi2) * pos.height + pos.get_x(), math.sin((pi/2) + (seg-2) / n * pi2) * pos.height + pos.get_y()) ) 
        corners.append( (math.cos( seg / n * pi2) * pos.side + pos.get_x(), math.sin(  seg / n * pi2) * radius + pos.get_y()) )
        corners.append( (math.cos((pi/2) + (seg - 1) / n * pi2) * pos.height + pos.get_x(), math.sin((pi/2) + (seg-1) / n * pi2) * pos.height + pos.get_y()) ) 
        pygame.draw.polygon(Surface, color, corners)



options = Options()

screen = pygame.display.set_mode(options.resolution)

pygame.display.set_caption('Quick Start')
manager = UIManager(options.resolution, 'data/themes/quick_theme.json')

button_reset = UIButton((10, 10), 'Reset')
button_exit = UIButton((80,10), 'Exit')


card_position = [1, 1]
placed_cards = []

map_info = dict()
map_color = dict()

rotation = 0

running = True
while running:
    for event in pygame.event.get():
        if event.type == UI_BUTTON_PRESSED:
            if event.ui_element == button_reset:
                map_color.clear()
                map_info.clear()
                placed_cards.clear()
            if event.ui_element == button_exit:
                running = False
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_h:
                pass
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
                running = False
            if event.key == pygame.K_RIGHT:
                card_position[0] += 1
            if event.key == pygame.K_LEFT:
                card_position[0] -= 1
            if event.key == pygame.K_DOWN:
                card_position[1] += 1
            if event.key == pygame.K_UP:
                card_position[1] -= 1
            if event.key == pygame.K_r:
                rotation += 1
                rotation %= 3
            if event.key == pygame.K_SPACE:
                if card_position in placed_cards:
                    placed_cards.remove(card_position)
                else:
                    placed_cards.append(card_position.copy())


                # normal hat
                for index, pos in enumerate(get_neigbours(card_position, rotation)):
                    if index == 0:
                        act_seg = [5, 0, 1, 2]
                    if index == 1:
                        act_seg = [2, 3]
                    if index == 2:
                        act_seg = [4, 5]

                    for seg in act_seg:
                        dict_entry = str(pos[0]) + ':' + str(pos[1])

                        if dict_entry in map_info:
                            segments = map_info[dict_entry]
                        else:
                            segments = []
                            map_color[dict_entry] = random.choice(COLOR_LIST)

                        seg += rotation*2
                        seg %= 6
                        if seg not in segments:
                            segments.append(seg)


                        map_info[dict_entry] = segments
                print(map_info)

        manager.process_events(event)

    # Fill the background with white
    screen.fill(BLACK)

    # BOARD
    radius = options.radius
    for y in range(1, int( options.resolution[1] /math.sqrt(3)/radius)):
        for x in range(1, int( options.resolution[0] /1.5/radius)):
            color = GREY

            for pos in placed_cards:
                if pos == [x,y]:
                    color = RED

                dict_entry = str(x)+':'+str(y)
                if dict_entry in map_info:
                    #print(dict_entry, map_info[dict_entry])
                    for segment in map_info[dict_entry]:
                        if dict_entry in map_color:
                            draw_segment(screen, map_color[dict_entry], radius, (x, y), [segment])
                        else:
                            draw_segment(screen, BLUE, radius, ( x , y ), [segment])


    #for y in range(1, options.resolution[1]):
    for y in range(1, 10):
        for x in range(1, 10):
            draw_hexagon(screen, color, radius, (x,y))

    draw_spectre(screen, WHITE, radius, card_position, rotation )

    manager.update(10)    
    manager.draw_ui(screen)
    pygame.display.update()

pygame.quit()
